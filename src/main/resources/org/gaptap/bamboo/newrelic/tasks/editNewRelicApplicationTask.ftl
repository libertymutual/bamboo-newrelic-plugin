[#import "/lib/ace.ftl" as ace ]

[#include "apiKeyFragment.ftl"]

[@ui.bambooSection titleKey='newrelic.config.application.section.title']
	[@ww.textfield labelKey='newrelic.config.application.applicationId' name='applicationId' required='true' /]
	
	[@ww.select labelKey='newrelic.config.application.data.options' name='dataOption'
	                                        listKey='key' listValue='value' toggle='true'
	                                        list=dataOptions /]
	
	[@ui.bambooSection dependsOn="dataOption" showOn="0"]
		[@ww.textfield labelKey='newrelic.config.application.requestFile' name='requestFile' required='true' cssClass="long-field" /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn="dataOption" showOn="1"]
		[@ace.textarea labelKey='newrelic.config.application.inlineData' name="inlineData" required=true/]
	[/@ui.bambooSection]
[/@ui.bambooSection]

[#include "advancedFragment.ftl"]