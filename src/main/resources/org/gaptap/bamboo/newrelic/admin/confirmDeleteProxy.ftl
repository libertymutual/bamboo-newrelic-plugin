
<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="newrelic.admin.sharedProxy.heading" /]
	[@ui.clear /]
	[@ww.form action='/admin/newrelic/deleteSharedProxy.action'
			submitLabelKey='newrelic.admin.sharedProxy.button.delete'
			cancelUri='/admin/newrelic/configuration.action'
			showActionErrors='true']

		[@ww.hidden name='mode' /]
		[@ww.hidden name='proxyId' /]

		[@ui.messageBox type="warning" titleKey="newrelic.admin.sharedProxy.delete.confirm.title"]
		    [@ww.text name="newrelic.admin.sharedProxy.delete.confirm.message"]
		    [/@ww.text]
		[/@ui.messageBox]
	[/@ww.form]
</body>
</html>