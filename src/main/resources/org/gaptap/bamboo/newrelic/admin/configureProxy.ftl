
[#if mode == 'edit' ]
	[#assign targetAction = "/admin/newrelic/updateSharedProxy.action"]
	[#assign submitButtonKey = "newrelic.admin.sharedProxy.button.update"]
[#else]
	[#assign targetAction = "/admin/newrelic/createSharedProxy.action"]
	[#assign submitButtonKey = "newrelic.admin.sharedProxy.button.create"]
[/#if]

<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="newrelic.admin.sharedProxy.heading" /]
	[@ui.clear /]
	[@ww.form action=targetAction
			submitLabelKey=submitButtonKey
			titleKey='newrelic.admin.sharedProxy.form.title'
			cancelUri='/admin/newrelic/configuration.action'
			showActionErrors='true'
			descriptionKey='newrelic.admin.sharedProxy.form.overview']
		[@ww.hidden name='mode' /]
		[#if mode == 'edit']
			[@ww.hidden name='proxyId' /]
		[/#if]
		[@ww.textfield name='name' labelKey='newrelic.admin.sharedProxy.form.name' descriptionKey='newrelic.admin.sharedProxy.form.name.description' required='true' /]
		[@ww.textfield name='host' labelKey='newrelic.admin.sharedProxy.form.host' required='true' /]
		[@ww.textfield name='port' labelKey='newrelic.admin.sharedProxy.form.port' required='true' /]
		[@ww.textfield name='description' labelKey='newrelic.admin.sharedProxy.form.description' descriptionKey='newrelic.admin.sharedProxy.form.description.description' required='false' /]
	[/@ww.form]
</body>
</html>
