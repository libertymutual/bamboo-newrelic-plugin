
[#if mode == 'edit' ]
	[#assign targetAction = "/admin/newrelic/updateSharedApiKey.action"]
	[#assign submitButtonKey = "newrelic.admin.sharedApiKey.button.update"]
[#else]
	[#assign targetAction = "/admin/newrelic/createSharedApiKey.action"]
	[#assign submitButtonKey = "newrelic.admin.sharedApiKey.button.create"]
[/#if]

<html>
<head>
	<meta name="decorator" content="adminpage">
</head>
<body>
	[@ui.header pageKey="newrelic.admin.sharedApiKey.heading" /]
	[@ui.clear /]
	[@ww.form action=targetAction
			submitLabelKey=submitButtonKey
			titleKey='newrelic.admin.sharedApiKey.form.title'
			cancelUri='/admin/newrelic/configuration.action'
			showActionErrors='true'
			descriptionKey='newrelic.admin.sharedApiKey.form.overview']
		[@ww.hidden name='mode' /]
		[#if mode == 'edit']
			[@ww.hidden name='apiKeyId' /]
		[/#if]
		[@ww.textfield name='name' labelKey='newrelic.admin.sharedApiKey.form.name' descriptionKey='newrelic.admin.sharedApiKey.form.name.description' required='true' /]
		[@ww.textfield name='apiKey' labelKey='newrelic.admin.sharedApiKey.form.apiKey' descriptionKey='newrelic.admin.sharedApiKey.form.apiKey.description' required='true' /]
		[@ww.textfield name='description' labelKey='newrelic.admin.sharedApiKey.form.description' descriptionKey='newrelic.admin.sharedApiKey.form.description.description' required='false' /]
	[/@ww.form]
</body>
</html>
