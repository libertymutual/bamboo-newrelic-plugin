/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.deployment.admin;

import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * @author David Ehringer
 */
@SuppressWarnings("serial")
public class ManageApiKeysAction extends BaseAdminAction {

    private int apiKeyId;
    private String name;
    private String apiKey;
    private String description;

    private NewRelicAdminService adminService;

    public ManageApiKeysAction(NewRelicAdminService adminService) {
        this.adminService = adminService;
    }

    public List<SharedApiKey> getApiKeys() {
        return adminService.allSharedApiKeys();
    }

    public List<SharedProxyServer> getProxies(){
        return adminService.allSharedProxyServers();
    }
    
    public String doCreate() throws Exception {
        if (!isValidApiKey()) {
            setMode(ADD);
            addActionError(getText("newrelic.admin.validation.errors"));
            return ERROR;
        }
        adminService.addSharedApiKey(name, apiKey, description);
        return SUCCESS;
    }

    public String doEdit() {
        setMode(EDIT);
        SharedApiKey sharedApiKey = adminService.getSharedApiKey(apiKeyId);
        name = sharedApiKey.getName();
        apiKey = sharedApiKey.getApiKey();
        description = sharedApiKey.getDescription();
        return EDIT;
    }

    public String doUpdate() throws Exception {
        if (!isValidApiKey()) {
            setMode(EDIT);
            addActionError(getText("newrelic.admin.validation.errors"));
            return ERROR;
        }
        adminService.updateSharedApiKey(apiKeyId, name, apiKey, description);
        return SUCCESS;
    }
    
    public String doConfirmDelete(){
        return SUCCESS;
    }
    
    public String doDelete(){
        // TODO check exists first
        adminService.deleteSharedApiKey(apiKeyId);
        return SUCCESS;
    }

    private boolean isValidApiKey() {
        if (StringUtils.isBlank(name)) {
            addFieldError("name", getText("newrelic.config.required.field"));
        }
        if (StringUtils.isBlank(apiKey)) {
            addFieldError("apiKey", getText("newrelic.config.required.field"));
        }
        return !(hasFieldErrors() || hasActionErrors());
    }

    public int getApiKeyId() {
        return apiKeyId;
    }

    public void setApiKeyId(int apiKeyId) {
        this.apiKeyId = apiKeyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
