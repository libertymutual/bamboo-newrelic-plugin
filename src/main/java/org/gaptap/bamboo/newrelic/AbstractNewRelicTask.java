/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic;

import static org.gaptap.bamboo.newrelic.NewRelicTaskDataProvider.SHARED_API_KEY;
import static org.gaptap.bamboo.newrelic.NewRelicTaskDataProvider.SHARED_PROXY_HOST;
import static org.gaptap.bamboo.newrelic.NewRelicTaskDataProvider.SHARED_PROXY_PORT;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.API_KEY;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.API_KEY_OPTION;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.API_KEY_OPTION_LOCAL;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.API_KEY_OPTION_SHARED;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.CUSTOM_PROXY;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.CUSTOM_PROXY_ENABLED;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.PROXY_HOST;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.PROXY_OPTION;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.PROXY_OPTION_LOCAL;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.PROXY_OPTION_SHARED;
import static org.gaptap.bamboo.newrelic.deployment.NewRelicDeploymentTaskConfigurator.PROXY_PORT;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;

/**
 * @author David Ehringer
 */
public abstract class AbstractNewRelicTask implements CommonTaskType {

    protected boolean isDeploymentProject(CommonTaskContext commonTaskContext) {
        return commonTaskContext instanceof DeploymentTaskContext;
    }

    protected String getApiKey(CommonTaskContext commonTaskContext) {
        String apiKeyOption = commonTaskContext.getConfigurationMap().get(API_KEY_OPTION);
        String apiKey = null;
        if (API_KEY_OPTION_SHARED.equals(apiKeyOption)) {
            apiKey = commonTaskContext.getRuntimeTaskContext().get(SHARED_API_KEY);
        } else if (API_KEY_OPTION_LOCAL.equals(apiKeyOption) || apiKeyOption == null) {
            // In original versions of the plugin, API_KEY_OPTION didn't exist
            // so both null and API_KEY_OPTION_LOCAL are equivalent
            apiKey = commonTaskContext.getConfigurationMap().get(API_KEY);
        }
        return apiKey;
    }

    protected boolean useProxy(CommonTaskContext commonTaskContext) {
        return CUSTOM_PROXY_ENABLED.equals(commonTaskContext.getConfigurationMap().get(CUSTOM_PROXY));
    }

    protected ConnectionParams getConnectionParams(CommonTaskContext commonTaskContext) {
        ConfigurationMap configurationMap = commonTaskContext.getConfigurationMap();
        String proxyHost = null;
        int proxyPort = 80;
        String proxyOption = configurationMap.get(PROXY_OPTION);
        if (PROXY_OPTION_LOCAL.equals(proxyOption)) {
            proxyHost = configurationMap.get(PROXY_HOST);
            proxyPort = Integer.parseInt(configurationMap.get(PROXY_PORT));
        } else if (PROXY_OPTION_SHARED.equals(proxyOption)) {
            proxyHost = commonTaskContext.getRuntimeTaskContext().get(SHARED_PROXY_HOST);
            proxyPort = Integer.parseInt(commonTaskContext.getRuntimeTaskContext().get(SHARED_PROXY_PORT));
        } else {
            throw new IllegalArgumentException("Programming error. Invalid proxy option: " + proxyOption);
        }
        return new ConnectionParams(proxyHost, proxyPort);
    }

}
