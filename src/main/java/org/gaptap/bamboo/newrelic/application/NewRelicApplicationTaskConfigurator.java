/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.application;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.gaptap.bamboo.newrelic.AbstractNewRelicTaskConfigurator;
import org.gaptap.bamboo.newrelic.deployment.admin.NewRelicAdminService;
import org.gaptap.bamboo.newrelic.deployment.admin.SharedApiKey;
import org.gaptap.bamboo.newrelic.deployment.admin.SharedProxyServer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.opensymphony.xwork.TextProvider;

/**
 * @author David Ehringer
 */
public class NewRelicApplicationTaskConfigurator extends AbstractNewRelicTaskConfigurator {

    public static final String APP_ID = "applicationId";
    
    public static final String SELECTED_DATA_OPTION = "dataOption";
    public static final String DATA_OPTION_FILE = "0"; 
    public static final String DATA_OPTION_INLINE = "1";
    private static final String DATA_OPTION_DEFAULT = DATA_OPTION_FILE;
    public static final String REQUEST_FILE = "requestFile";
    public static final String INLINE_DATA = "inlineData";
    
    private static final String INLINE_DATA_EXAMPLE = "{\n  \"application\": {\n    \"name\": \"My Application\",\n    \"settings\": {\n      \"app_apdex_threshold\": 1.0,\n      \"end_user_apdex_threshold\": 1.0,\n      \"enable_real_user_monitoring\": true\n    }\n  }\n}";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(APP_ID, REQUEST_FILE, SELECTED_DATA_OPTION, INLINE_DATA);

    public NewRelicApplicationTaskConfigurator(NewRelicAdminService newRelicAdminService, TextProvider textProvider,
            TaskConfiguratorHelper taskConfiguratorHelper) {
        super(newRelicAdminService, textProvider, taskConfiguratorHelper);
    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params,
            @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
        context.put(SELECTED_DATA_OPTION, DATA_OPTION_DEFAULT);
        context.put(INLINE_DATA, INLINE_DATA_EXAMPLE);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context,
            @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }
    
    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> apiKeyOptions = Maps.newLinkedHashMap();
        apiKeyOptions
                .put(DATA_OPTION_FILE, textProvider.getText("newrelic.config.application.data.option.file"));
        apiKeyOptions.put(DATA_OPTION_INLINE,
                textProvider.getText("newrelic.config.application.data.option.inline"));
        context.put("dataOptions", apiKeyOptions);
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        if (StringUtils.isEmpty(params.getString(APP_ID))) {
            errorCollection.addError(APP_ID, textProvider.getText("newrelic.config.required.field"));
        }
        if(DATA_OPTION_FILE.equals(params.getString(SELECTED_DATA_OPTION))){
            if (StringUtils.isEmpty(params.getString(REQUEST_FILE))) {
                errorCollection.addError(REQUEST_FILE, textProvider.getText("newrelic.config.required.field"));
            }
        }else if(DATA_OPTION_INLINE.equals(params.getString(SELECTED_DATA_OPTION))){
            if (StringUtils.isEmpty(params.getString(INLINE_DATA))) {
                errorCollection.addError(INLINE_DATA, textProvider.getText("newrelic.config.required.field"));
            }
        }else{
            errorCollection.addError(SELECTED_DATA_OPTION, textProvider.getText("newrelic.config.required.field"));
        }
    }
}
