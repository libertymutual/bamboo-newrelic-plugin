/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.newrelic.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.gaptap.bamboo.newrelic.ConnectionParams;
import org.gaptap.bamboo.newrelic.NewRelicApiException;

import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.LogInterceptorStack;
import com.atlassian.bamboo.plan.PlanResultKey;

/**
 * @author David Ehringer
 */
public class DefaultApplicationUpdater implements ApplicationUpdater {

    @Override
    public void update(ApplicationInfo applicationInfo, BuildLogger buildLogger) {
        HttpClient client = new DefaultHttpClient();
        doUpdate(client, applicationInfo, buildLogger);
    }

    @Override
    public void update(ApplicationInfo applicationInfo, ConnectionParams connectionParams, BuildLogger buildLogger) {
        HttpClient client = new DefaultHttpClient();
        HttpHost proxy = new HttpHost(connectionParams.getProxyHost(), connectionParams.getProxyPort());
        client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
        doUpdate(client, applicationInfo, buildLogger);
    }

    private void doUpdate(HttpClient client, ApplicationInfo applicationInfo, BuildLogger buildLogger)
            throws NewRelicApiException {
        HttpPut put = new HttpPut("https://api.newrelic.com/v2/applications/" + applicationInfo.getApplicationId()
                + ".json");
        put.setHeader("X-Api-Key", applicationInfo.getApiKey());

        HttpEntity entity = applicationInfo.getHttpEntity();
        put.setEntity(entity);

        StringBuilder result = new StringBuilder();
        try {
            buildLogger.addBuildLogEntry("POSTing application update to New Relic");
            HttpResponse response = client.execute(put);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            int responseCode = response.getStatusLine().getStatusCode();
            if (200 == responseCode) {
                buildLogger.addBuildLogEntry("Successfully updated application");
                buildLogger.addBuildLogEntry("Response: " + result.toString());
            } else {
                buildLogger.addErrorLogEntry("New Relic API response (HTTP " + responseCode + "): ");
                buildLogger.addErrorLogEntry(result.toString());
                handleError(responseCode, buildLogger);
            }
        } catch (IOException e) {
            String message = "Unable to POST application update to New Relic for " + applicationInfo.toString();
            buildLogger.addErrorLogEntry(message, e);
            throw new NewRelicApiException(message, e);
        }
    }

    private void handleError(int responseCode, BuildLogger buildLogger) {
        if (400 == responseCode) {
            buildLogger.addErrorLogEntry("API returned unauthorized. Check you API key.");
        } else if (422 == responseCode) {
            buildLogger.addErrorLogEntry("Validation error occurred when updating the application.");
        } else if (404 == responseCode) {
            buildLogger.addErrorLogEntry("No Application found with the given ID");
        }
        throw new NewRelicApiException("Unable to update application in New Relic. Please check the logs for details.");
    }

}
